package ru.dshalamov.test_2gis

import java.util.concurrent.{ConcurrentLinkedQueue, ForkJoinPool}
import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors


import ru.dshalamov.test_2gis.job.Job

import scala.concurrent.{ExecutionContextExecutorService, Future}

class Worker(maxParallelJobs: Int)(implicit queue: ConcurrentLinkedQueue[Job]) extends Thread {

  /**
    * @inheritdoc
    */
  override def run(): Unit = {
    implicit val ec: ExecutionContextExecutorService =
      ExecutionContext.fromExecutorService(Executors.newWorkStealingPool(maxParallelJobs))

    while (!this.isInterrupted) {
      queue.poll() match {
        case job: Job => Future {
          job.run()
        }
        case _ =>
      }
    }
  }
}
