package ru.dshalamov.test_2gis

import ru.dshalamov.test_2gis.job.Job
import com.sun.net.httpserver.HttpServer
import scala.concurrent.duration.Duration
import java.net.InetSocketAddress
import java.util.concurrent.ConcurrentLinkedQueue

object Crawler extends App {
  implicit var queue: ConcurrentLinkedQueue[Job] = new ConcurrentLinkedQueue[Job]()

  val worker = new Worker(100)

  try {
    if (args.length < 2) {
      throw new IllegalArgumentException("Arguments host and port is required")
    }

    worker.start()

    val server = HttpServer.create(new InetSocketAddress(args(0), args(1).toInt), 0)
    server.createContext("/", new RequestHandler(Duration("10 seconds"), 10))
    server.setExecutor(null)
    server.start()
    println("Hit any key to exit...")
    System.in.read()
    server.stop(0)
  } catch {
    case e: Throwable => println(e.getMessage)
  } finally {
    worker.interrupt()
  }
}