package ru.dshalamov.test_2gis

import java.util.concurrent.{ConcurrentLinkedQueue, Executors}

import scala.concurrent.duration._
import com.sun.net.httpserver.{HttpExchange, HttpHandler}

import ru.dshalamov.test_2gis.job.result.{Failure, Result, Success}
import org.json4s._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.read
import ru.dshalamov.test_2gis.job.{Job, TitleParser}

import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutorService, Future}
import scala.tools.nsc.interpreter.InputStream


/**
  * @param timeout - request timeout
  * @param maxParallelRequest - max requests per time
  * @param queue - jobs queue
  */
class RequestHandler(timeout: Duration, maxParallelRequest: Int = 5)(implicit queue: ConcurrentLinkedQueue[Job]) extends HttpHandler {
  implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats
  implicit val executionContext: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(Executors.newWorkStealingPool(maxParallelRequest))

  /**
    * @inheritdoc
    */
  override def handle(exchange: HttpExchange): Unit = Future {
    sendResponse(
      exchange,
      getTitles(parseRequest(exchange.getRequestBody))
    )
  } onComplete {
    case scala.util.Failure(res) => sendResponse(exchange, res.getMessage, 500)
  }

  /**
    * @param urls - the set of target urls
    * @return
    */
  private def getTitles(urls: Set[String]): Map[String, Any] = {
    var urlsSeq = urls.toSeq

    //create jobs
    val jobs = urls.map(TitleParser(_))

    //push into queue
    jobs.foreach(queue.add(_))

    //wait result and return
    Await.result(Future.sequence(jobs.map(_.future)), timeout)
      .zipWithIndex
      .map {
        case (result: Result, index: Int) => result match {
          case Success(job, title) => job.name -> title
          case Failure(job, title) => job.name -> false
        }
      }
      .toMap
  }

  /**
    *
    * @param exchange - the Http exchange
    * @param response - response body
    */
  private def sendResponse(exchange: HttpExchange, response: String, code: Int = 200) {
    exchange.getResponseHeaders.add("Content-Type", "application/json")
    exchange.sendResponseHeaders(code, response.getBytes().length)
    exchange.getResponseBody.write(response.getBytes())
    exchange.getResponseBody.close()
  }

  /**
    *
    * @param data - input data
    * @return
    */
  private def parseRequest(data: InputStream): Set[String] =
    read[Set[String]](scala.io.Source.fromInputStream(data).mkString).filter(_.matches("^https?://.*"))

  /**
    *
    * @param results - handling result
    * @return
    */
  implicit def resultsToJsob(results: Map[String, Any]): String = Serialization.write(results)
}