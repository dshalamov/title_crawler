package ru.dshalamov.test_2gis.job.result

import ru.dshalamov.test_2gis.job.Job

case class Success(job: Job, result: String) extends Result {

}
