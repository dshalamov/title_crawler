package ru.dshalamov.test_2gis.job.result

import ru.dshalamov.test_2gis.job.Job

case class Failure(job: Job, error: String) extends Result {

}
