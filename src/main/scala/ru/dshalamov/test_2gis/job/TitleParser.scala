package ru.dshalamov.test_2gis.job

import java.net.URL
import ru.dshalamov.test_2gis.job.result.{Result, Success}

object TitleParser {
  def apply(url: String): TitleParser = new TitleParser(url)
}

/**
  * @param url - target url
  */
class TitleParser(url: String) extends Job {
  var name: String = url
  val timeout = 1000

  private val titleRegexp = "<title>(.*?)</title>".r("title")

  override def doJob(): Result = {
    val conn = new URL(url).openConnection()
    conn.setConnectTimeout(timeout)
    conn.setReadTimeout(timeout)
    val html = scala.io.Source.fromInputStream(conn.getInputStream).mkString

    titleRegexp.findFirstMatchIn(html) match {
      case Some(matches) => Success(this, matches.group("title"))
      case None => throw new Error("Invalid html")
    }
  }
}
