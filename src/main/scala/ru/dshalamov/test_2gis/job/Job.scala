package ru.dshalamov.test_2gis.job

import ru.dshalamov.test_2gis.job.result.{Failure, Result}

import scala.concurrent.{Future, Promise}
import scala.util.Try

/**
  * Abstract job
  */
trait Job extends Promise[Result] {
  var name: String

  /**
    *
    */
  final def run(): Unit = {
    try {
      this success doJob()
    } catch {
      case e: Throwable => this complete Try(Failure(this, e.getMessage))
    }
  }

  /**
    *
    * @return
    */
  def doJob(): Result

  /**** Promise implementation *******/

  private val p = Promise[Result]()

  override def future: Future[Result] = p.future

  override def isCompleted: Boolean = p.isCompleted

  override def tryComplete(result: Try[Result]): Boolean = p.tryComplete(result)
}
